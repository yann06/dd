<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
  </head>
  
  <?php
  
  
    if( isset($_POST['damage_token']) )
	{   
		
		$dbc = mysqli_connect("localhost", "root","","ddtools");
	/* Vérification de la connexion */
if ($dbc->connect_errno) {
    printf("Échec de la connexion : %s\n", $dbc->connect_error);
    exit();
}
	/* Character Load */
     $character=$_POST['damage_to'];
	 $damageTaken=$_POST['damage_token'];
	 $q='SELECT * FROM characters WHERE name="'.$character.'"';
	 $results=mysqli_query($dbc,$q);
	 mysqli_error($dbc);
	 echo mysqli_error($dbc);
	 $character_stats = mysqli_fetch_assoc($results);
	 // stats stockées dans $character_stats
	 $currentHpBuffer=$character_stats["current_hp"];
	 $hpBuffer=$currentHpBuffer-$damageTaken;
	 if($hpBuffer<0)
	 {
		 $hpBuffer=0;
	 }
	 $q='UPDATE characters SET current_hp="'.$hpBuffer.'" WHERE name="'.$character.'"';
	 $results2=mysqli_query($dbc,$q);
	 mysqli_error($dbc);
	 echo mysqli_error($dbc);
	 $q='SELECT * FROM characters WHERE name="'.$character.'"';
	 $results=mysqli_query($dbc,$q);
	 mysqli_error($dbc);
	 echo mysqli_error($dbc);
	 $character_stats = mysqli_fetch_assoc($results);
	}

	/* Check if Character is Loaded*/
	if( isset($_POST['characterInput']) )
{
	$dbc = mysqli_connect("localhost", "root","","ddtools");
	/* Vérification de la connexion */
if ($dbc->connect_errno) {
    printf("Échec de la connexion : %s\n", $dbc->connect_error);
    exit();
}
	/* Character Load */
     $character=$_POST['characterInput'];
	 $q='SELECT * FROM characters WHERE name="'.$character.'"';
	 $results=mysqli_query($dbc,$q);
	 mysqli_error($dbc);
	 echo mysqli_error($dbc);
	 $character_stats = mysqli_fetch_assoc($results);
}

?>

  <body>
<div class="container">
  <div class="row">
 
    <div class="col col-md-10 offset-md-2" name="content-block">
			<h1 align="center"> D&D Edition 5 Tools Set </h1>
    </div>
 </div>
 <div class="row">
	 <?php include 'menu.php';?>

 <div class="col col-md-10">
 <br/>
 

 
 <?php 
        /* To Display if Character IS NOT Loaded */
		if (!isset($_POST['characterInput']) && !isset($_POST['damage_token']) )
{
 echo('
<form class="form-inline" method="post" action="character_sheet.php">
  <label class="mr-sm-2" for="inlineFormCustomSelect">Select a character to load</label>
  <select name="characterInput" class="custom-select mb-2 mr-sm-2 mb-sm-0" id="inlineFormCustomSelect">
    <option value="kepesk">Kepesk</option>
    <option value="garrett">Garrett</option>
  </select>
  <button  type="submit" class="btn btn-primary">Submit</button>
</form>

	 <br/> 
');
}	 
        /* To Display if Character IS Loaded */
 	if (isset($_POST['characterInput']) || isset($_POST['damage_token']) ){
$maxHP=$character_stats["max_hp"]; // link to character max hp attribute
$currentHP=$character_stats["current_hp"];
$hpBarWidth=$currentHP*100/$maxHP;

echo('

<div> Name : <blue> '.$character_stats["name"].'</blue></div>
<br/>
<div class="progress">
		<div class="progress-bar progress-bar-striped bg-danger" style="width: '.$hpBarWidth.'%; role="progressbar" aria-valuenow="'.$currentHP.'" aria-valuemin="0" aria-valuemax="'.$maxHP.'">
		Current HP : '.$currentHP.'/'.$maxHP.'
		</div>
	</div>
	 <br>
	 
	<form class="form-inline" method="post" action="character_sheet.php">
  <label class="col-form-label">Damage Taken  :</label>
    <input type="hidden" name="damage_to" value="'.$character.'">
    <input name="damage_token" type="text" class="form-control col-1" >
  <button  type="submit" class="btn btn-primary">Submit</button>
</form>

	
');
	}

 ?>


 </div>
</div>
 </div>
   

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
  </body>
</html>